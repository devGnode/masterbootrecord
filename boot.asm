; compile with nasm
; 2017 BASIC MBR 16/32 Bits
[BITS 16]
[ORG 0x0]

; Floppy sector
%define SECT_SZ 512

%define BOOTSEG 0x07c0 ; BIOS ROUTINE ADDR
%define SS_BASE 0x8000 ; STACK BASE ADDR
%define SS_SIZE 0x0f00 ; STACK SIZE

%define SYS_SEG 0x1000 ; system loaded to sector 1

	global _start

section .data
section .bss
section .text

	; main
	jmp _start
	db "BOOT",0 ; MAGIC_BOOT_SECTOR ;)

; reset
; Screen
reset:
	mov ax, 0x0003
	mov byte[row],0
	int 0x10
	ret
_start:
	; INIT SEGMENTATION
	; SEG DS    : Program data MBR 0x07c0
	; SEG ES    : Progam multi-segment
	; SEG SI DI : Ptr src, dst
	mov ax, BOOTSEG
	mov ds, ax
	mov es, ax
	xor di,di
	xor si,si

	; STACK SEG INIT
	; SIZE : 0x1000 bytes
	mov ax, SS_BASE
	mov ss, ax
	mov sp, 0xf000

	; reset screen
	call reset

	; bootMgr
	mov ax, 0x1301
	mov cx, 0x17
	mov bx, 0x0002
	mov bp, bootMgr
	mov dh, byte[row]
	inc byte[row]
	int 0x10

	; load kernel
	call kernelLoad

end:
	jmp end

badLoad:
	mov ax, 0x1301
	mov cx, 0x1c
	mov bx, 0x0002
	mov bp, bootErrorMgr
	mov dh, byte[row]
	inc byte[row]
	int 0x10
	jmp end
	ret

kernelLoad:
	xor ax, ax
	int 0x13

	mov al, 0x05
	push ax
	push es
	; init segement
	; 0x0001:0000
	mov ax, SYS_SEG
	mov es, ax
	mov bx, 0x00

    	mov ax, 0x0201
   	mov cx, 0x0002
   	xor dx, dx
    	int 0x13
	pop es
	jc badLoad

	; saut vers le kernel
	call reset
    	jmp dword SYS_SEG:0
	ret

; VARIABLE HERE
; si ont dispose les vars  dans le
; segment : .data les deux derniers
; octects ne seront pas 0x55AA mais
; la section .data donc Boot Failed.
bootMgr:
	db "Loading basic kernel...",0

bootErrorMgr:
	db "[ BAD] Bad sector ! Halted !",0

row:
	db 0
drive:
	db 0
buffer:
	resb 0xff

	; Bourrage
	; 510 bytes + 2 bytes e_magic
	times 510-($-$$) db 0
	dw 0xAA55