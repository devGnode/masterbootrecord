
all: mount

mount: clean mbr
	cat boot.bin kernel.o | dd of=boot.img bs=512 count=2880
	@hexdump -Cv boot.img
	
mbr:
	@nasm -f bin -o boot.bin boot.s
	@nasm -f bin -o kernel.o ./kernel/head.s
	@hexdump -vC boot.bin
	@echo "[ ndisasm ] boot sector sector=1 bs=512"
	@ndisasm boot.bin

clean:
	rm -f boot.bin
