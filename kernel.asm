; Print Function
; bh = color
[BITS 16]
[ORG 0x0]

global __main__

%define DS_SEG 0x1000 ; DS ES SEG
%define SS_SEG 0x8000 ;
%define SP_SEG 0x0f00 ;

section .data
	kernelLoaded:
		db "[ OK ] Kernel is loaded !",13,10,0
	ps:
		db "wos@free:/$ ",0
	buffer:
		resb 	0xFF
	crlf:
		db 13,10,0
section .bss
section .text

	jmp __main__
; param si = string offset
; return cx
strlen:
	xor cx, cx
	.loopstr:
	lodsb
	cmp al,0
	je .endstr
	inc cx
	jmp .loopstr
.endstr:
	ret

print:
	push	ax
	push	bx
	mov	ah, 0x0E
	xor 	bh,bh
	mov 	dx, 0x0002
.loop:
	lodsb
	cmp	al,0
	je	pend
	int	0x10
	jmp	.loop
pend:
	pop	ax
	pop	bx
	ret

; memset
; param al = ASCII
; 	cx = buffer size
;	si = buffer Addr
memset:
.loopmem:
	mov [si],al
	dec cx
	inc si
	cmp cx, 0
	je .endmemset
	jmp .loopmem
.endmemset:
	ret

__main__:

	; INIT DATA SEG
	mov ax, DS_SEG
	mov ds, ax
	mov es, ax
	xor si, si
	xor di, di

	; INIT STACK
	mov ax, SS_SEG
	mov ss, ax
	mov sp, SP_SEG

	mov bl, 0x02
	mov si, kernelLoaded
	call print

; prompt
prompt:
	; reset buffer prompt
	mov ax, 0x00
	mov si, buffer
	mov cx, 0xff
	call memset

	; PS
	mov si, ps
	mov bl, 0x02
	call print

.loopread:

	xor ax, ax
	int 0x16
	cmp al, 0x0D
	je .endread
	cmp cx, 0xff
	je .loopread
	mov [si], al
	inc si

	mov ah, 0x0E
	mov bx, 0x0002
	int 0x10
	inc cx
	jmp .loopread

.endread:
	push esi
	mov bl, 0x02
	mov si, crlf
	call print

	mov bl, 0x02
	mov si, buffer
	call print

	mov bl, 0x02
	mov si, crlf
	call print

loopback:
	jmp prompt