# Run boot IMG


+ Download Bochs debugger : [Bochs - Download page](https://bochs.sourceforge.io/getcurrent.html)


# Bochs

Copié le fichier bochsrc.txt dans le dossier dossier d'installation de bochs.
Ligne 646, ajouter le chemin de boot.img qui ce trouve à la racine de ce repo.



Puis lancer Bochs.

<img src="https://i.ibb.co/wzrRQ42/a.png">

# Licence 

Tout les fichier présent sont sous licence AGPL, le repository racine de ce project
implique une licence AGPL, il est donc strictement interdit de faire un usage commercial de ces fichiers.

Tout project qui serait entreprit à partir de cette example doit être par définition sous licence AGPL par héritage.
En accord avec AGPL, Le nom de du propriétaire doit y être inclus dans chaque modification ou sous-release. 
 